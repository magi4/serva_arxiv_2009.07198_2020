# Serva_arXiv_2009.07198_2020

Contains input files and data used to generate the figures of the article:

Structural and dynamic properties of soda-lime-silica in the liquid phase

Alessandra Serva, Allan Guerault, Yoshiki Ishii, Emmanuelle Gouillart, Ekaterina Burov, and Mathieu Salanne,
*arXiv*, 2009.07198, 2020

[https://arxiv.org/abs/2009.07198](https://arxiv.org/abs/2009.07198)

The folder *typical_input_files* contains 2 input files for MetalWalls, which is available [here](https://gitlab.com/ampere2/metalwalls). Please note that a different code was used to generate the original data of the article.

The folder *DATA_ASCII_FIGURES* contains the processed data used to plot all the figures of the article.

